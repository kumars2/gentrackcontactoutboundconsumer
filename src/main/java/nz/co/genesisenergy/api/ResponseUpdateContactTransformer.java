package nz.co.genesisenergy.api;

import java.util.List;

import org.mule.api.MuleMessage;
import org.mule.api.transformer.TransformerException;
import org.mule.transformer.AbstractMessageTransformer;

import nz.co.genesisenergy.esb.customer.updatecontact.v1.EnumResponseStatus;
import nz.co.genesisenergy.esb.customer.updatecontact.v1.FaultObject;
import nz.co.genesisenergy.esb.customer.updatecontact.v1.UpdateContactResponse;
import nz.co.genesisenergy.esb.customer.updatecontact.v1.Error;

public class ResponseUpdateContactTransformer extends AbstractMessageTransformer{

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	/**
	 * We already have the jaxb object, so no requirement to unmarshall here
	 */
	@Override
	public Object transformMessage(MuleMessage message, String outputEncoding) throws TransformerException {
		String returnString = "";
		try {
			returnString = message.getPayloadAsString();
			System.out.println("message payload:" + message.getPayloadAsString());
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		UpdateContactResponse response = (UpdateContactResponse)message.getPayload() ;
		System.out.println("UpdateContactResponse: " + response.toString());

		EnumResponseStatus status = response.getResponseStatus();

		System.out.println("Response status: " + status.value());

		if("SUCCESS".equalsIgnoreCase(status.value())){
			return returnString;
		}else{
			FaultObject fault = response.getFault();
			List<nz.co.genesisenergy.esb.customer.updatecontact.v1.Error> errors = fault.getError();
			StringBuilder errorString = new StringBuilder("");
			for(Error e : errors){
				errorString.append("ErrorId: " + e.getId() + "\n");
				errorString.append("ErrorCode: " + e.getCode() + "\n");
				errorString.append("ErrorDetail: " + e.getDetail() + "\n");
				returnString = errorString.toString();
			}
			return returnString;
		}
	}

}
