package nz.co.genesisenergy.api;
import java.io.*;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.XMLGregorianCalendar;

import org.mule.api.MuleEventContext;
import org.mule.api.MuleMessage;
import org.mule.api.lifecycle.Callable;

import com.esotericsoftware.minlog.Log;
import com.fasterxml.jackson.databind.ObjectMapper;

import Event.Endpoint;
import Event.Event;
import Event.EventType;
import Event.Extensions;
import Event.Payload;
import contact.CanonicalContact;
import contact.SystemId;
import jdk.nashorn.internal.objects.Global;
import nz.co.genesisenergy.esb.customer.updatecontact.v1.CommunicationDetail;
import nz.co.genesisenergy.esb.customer.updatecontact.v1.UpdateContact;
import nz.co.genesisenergy.common.GlobalConstants;
import nz.co.genesisenergy.common.Util;
import nz.co.genesisenergy.esb.customer.updatecontact.v1.Brand;
import nz.co.genesisenergy.esb.customer.updatecontact.v1.Contact;

public class CMESBUpdateContactTransformer  implements Callable{

	@Override
	public Object onCall(MuleEventContext eventContext) throws Exception {

    	Event muleEvent = new Event();
    	MuleMessage message = eventContext.getMessage();
		try {

			muleEvent = (Event)message.getPayload();

	    	CanonicalContact canonicalContactEvent = new CanonicalContact();
	    	ObjectMapper mapper = new ObjectMapper();
	    	String encodedCanonicalString = muleEvent.getPayload().getContent();
	    	String decodedCanonical = PayloadBase64Decoder.getInstance().decode(encodedCanonicalString);
	    	
			canonicalContactEvent = mapper.readValue(decodedCanonical , CanonicalContact.class);

	    	String updateContactXML = "";
			String operation = muleEvent.getEventType().getOperation();
			//Take out in choice
			if(GlobalConstants.OPERATION_UPDATE.equalsIgnoreCase(operation)){
				updateContactXML = transformUpdate(canonicalContactEvent, message);
				message.setInvocationProperty(GlobalConstants.PROPERTY_OPERATION, GlobalConstants.OPERATION_UPDATE);
				return updateContactXML;
			}else{ 
				throw new Exception("Wrong object type, it should be an update contact.");
			}
		} catch (Exception e) {
			setErrorMuleEvent(message, muleEvent, e);
			throw new Exception ("Transformation Exception!");
		}
    }
    
    private void setErrorMuleEvent(MuleMessage message, Event muleEvent, Exception e) {
		Event errorMuleEvent = new Event();
		errorMuleEvent.setId(muleEvent.getId());
		errorMuleEvent.setCreationTime(new Date().toString());
		Endpoint provider = new Endpoint();
		provider.setId(GlobalConstants.MULE_PROVIDER_ID);
		provider.setName(GlobalConstants.SF_CONTACT_EVENT_PROVIDER);
		errorMuleEvent.setProvider(provider);
		EventType eventType = new EventType();
		eventType.setName(muleEvent.getEventType().getName());
		eventType.setObjectType(muleEvent.getEventType().getObjectType());
		eventType.setOperation(muleEvent.getEventType().getOperation());
		errorMuleEvent.setEventType(eventType);
		Payload errorPayload = new Payload();
		errorPayload.setContent(e.getStackTrace().toString());
		message.setPayload(errorPayload);
		errorMuleEvent.setPayload(errorPayload);
		Extensions extensions = new Extensions();
    	extensions.setAdditionalProperty(GlobalConstants.LAST_PAYLOAD, muleEvent.getPayload().getContent());
    	try{
    		String originalPayloadString = muleEvent.getExtensions().getAdditionalProperties().get(GlobalConstants.ORIGINAL_PAYLOAD);
    		extensions.setAdditionalProperty(GlobalConstants.ORIGINAL_PAYLOAD, originalPayloadString);
    	}catch(Exception ex){
    		Log.error("No extensions with original paylod in update");
    	}
    	errorMuleEvent.setExtensions(extensions);
	}

	public String transformUpdate(CanonicalContact cm, MuleMessage message) throws ParseException, DatatypeConfigurationException{

    	UpdateContact udpateContact = new UpdateContact();
    	UpdateContact.UpdateContactRequest request = new UpdateContact.UpdateContactRequest();
    	Brand brand = new Brand();
    	brand. setBrand(cm.getBrand().value());
    	Contact contact = new Contact();
    	contact.setBrand(brand);
    	contact.setId(cm.getCreatedById());
    	contact.setCustomerName("");
		message.setOutboundProperty(GlobalConstants.HEADER_X_BRAND_ID, brand.getBrand());
		message.setOutboundProperty(GlobalConstants.HEADER_X_USER_ID, cm.getLastModifiedById());
    	
    	XMLGregorianCalendar calendar = Util.getXMLDate(cm.getBirthDate());
    	contact.setDateOfBirth(calendar);
       	contact.setFirstName(cm.getFirstName());
    	contact.setMiddleName(cm.getMiddleName());
       	contact.setLastName(cm.getLastName());
       	contact.setPreferredName(cm.getPreferredName());
       	contact.setTitle(cm.getSalutation());
       	contact.setStatus(GlobalConstants.CONTACT_STATUS);

       	Long acountNumber = getGentrackAccountNumber(cm);
        contact.setNumber(acountNumber);

    	List<contact.CommunicationDetail> cmComDetails = cm.getCommunicationDetails();
    	for(contact.CommunicationDetail comDetail : cmComDetails){
    		
    		String cmPreference = comDetail.getPreference().value();
	    	if (GlobalConstants.HOME_PREFERENCE.equals(cmPreference) && 
					 ( GlobalConstants.EMAIL_PREFERENCE.equals(comDetail.getCommunicationType().value()) 
							 || GlobalConstants.FAX_PREFERENCE.equals(comDetail.getCommunicationType().value()))  ){
				 cmPreference = GlobalConstants.PRIVATE_PREFERENCE;
			 }
			 
			 CommunicationDetail cDetail = new CommunicationDetail();
			 cDetail.setType(comDetail.getCommunicationType().value());
			 cDetail.setPreference(cmPreference);
			 cDetail.setValue(comDetail.getValue());
			 //this is a reference, so no set method, just add
			 contact.getCommunicationDetail().add(cDetail);
    	}
    	
    	request.setContact(contact);
    	udpateContact.setUpdateContactRequest(request);
        return jaxbObjectToXML(udpateContact);
    }
    
    private Long getGentrackAccountNumber(CanonicalContact cm) {
    	List<SystemId> systemIds = cm.getSystemIds();
    	for(SystemId system : systemIds){
    		if(system.getSystem().GT.equals(system.getSystem())){
    	    	Long accountNumber = Long.parseLong(system.getId());
    			return accountNumber ;
    		}
    	}
    	
		return null;
	}

	private static String jaxbObjectToXML(UpdateContact request)
    {
        StringWriter sw = new StringWriter();
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(UpdateContact.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE); // To format XML
 
            jaxbMarshaller.marshal(request, sw);             
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        
        return sw.toString();
    }

    
}
