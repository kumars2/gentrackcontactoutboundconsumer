/**
 * 
 */
package nz.co.genesisenergy.common;

public class GlobalConstants {
	
	
	public final static String contactV1 = "http://www.genesisenergy.co.nz/esb/customer/updateContact/v1";
	public final static String HOME_PREFERENCE = "HOME";
	public final static String MOBILE_PREFERENCE = "MOBILE";
	public final static String EMAIL_PREFERENCE = "EMAIL";
	public final static String FAX_PREFERENCE = "FAX";
	public final static String PRIVATE_PREFERENCE = "PRIVATE";
	
	public final static String HEADER_X_BRAND_ID = "X-Brand-ID";
	public final static String HEADER_X_USER_ID = "X-USER-ID";
	public final static String GENE_GENTRACK = "GENE-Gentrack-";
	
	public final static String ROLE_TYPE_PRIMARY = "PRIMARY";
	public final static String ROLE_TYPE_RESIDENTIAL = "RESIDENTIAL";

	public final static String GT = "GT";
	public final static String CRM = "CRM";
	
	public final static String ORIGINAL_PAYLOAD = "OriginalPayload";
	public static final String MULE_PROVIDER_ID = "Mule";
	public static final String SF_CONTACT_EVENT_PROVIDER = "SalesforceContactEventPublisher";
	public static final String OPERATION_CREATE = "Create";
	public static final String PROPERTY_OPERATION = "operation";
	public static final String OPERATION_UPDATE = "Update";
	public static final String LAST_PAYLOAD = "LastPayload";
	public static final String CONTACT_STATUS = "status";
	
	
	
	
}
