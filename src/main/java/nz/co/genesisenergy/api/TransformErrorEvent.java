package nz.co.genesisenergy.api;

import java.util.Date;

import org.mule.api.MuleEvent;
import org.mule.api.MuleEventContext;
import org.mule.api.MuleException;
import org.mule.api.MuleMessage;
import org.mule.api.lifecycle.Callable;
import org.mule.api.processor.MessageProcessor;

import com.fasterxml.jackson.databind.ObjectMapper;

import Event.Endpoint;
import Event.Event;
import Event.EventType;
import Event.Extensions;
import Event.Payload;

public class TransformErrorEvent implements MessageProcessor{
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	@Override
	public MuleEvent process(MuleEvent event) throws MuleException {
    	Event muleEvent = new Event();
    	MuleMessage message = event.getMessage();

    	ObjectMapper muleEventmapper = new ObjectMapper();
		try {
			muleEvent = muleEventmapper.readValue(message.getPayloadAsString(), Event.class);
		} catch (Exception e) {
			e.printStackTrace();
		}

		Event errorMuleEvent = getErrorMuleEvent(message, muleEvent);
    	event.getMessage().setPayload(errorMuleEvent);
		return event;
	}

    private Event getErrorMuleEvent(MuleMessage message, Event muleEvent) {
		Event errorMuleEvent = new Event();
		errorMuleEvent.setId(muleEvent.getId());
		errorMuleEvent.setCreationTime(new Date().toString());
		
		Endpoint provider = new Endpoint();
		provider.setId("Mule");
		provider.setName("SalesforceContactEventPublisher");
		errorMuleEvent.setProvider(provider);
		
		EventType eventType = new EventType();
		eventType.setName(muleEvent.getEventType().getName());
		eventType.setObjectType(muleEvent.getEventType().getObjectType());
		eventType.setOperation(muleEvent.getEventType().getOperation());
		errorMuleEvent.setEventType(eventType);

		Payload errorPayload = new Payload();
		errorPayload.setContent("Transformation Error!");
		message.setPayload(errorPayload);
		errorMuleEvent.setPayload(errorPayload);

		Extensions extensions = new Extensions();
    	extensions.setAdditionalProperty("LastPayload", muleEvent.getPayload().getContent());
    	try{
    	String originalPayloadString = muleEvent.getExtensions().getAdditionalProperties().get("OriginalPayload");
    	extensions.setAdditionalProperty("OriginalPayload", originalPayloadString);
    	}catch (Exception e){
    		System.out.println("No original payload avaialbale");
    	}
    	errorMuleEvent.setExtensions(extensions);
    	return errorMuleEvent;
    	
	}


}
