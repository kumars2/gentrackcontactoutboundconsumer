package nz.co.genesisenergy.api;
import java.io.*;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.XMLGregorianCalendar;

import org.mule.api.MuleEventContext;
import org.mule.api.MuleMessage;
import org.mule.api.lifecycle.Callable;
import com.esotericsoftware.minlog.Log;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import Event.Endpoint;
import Event.Event;
import Event.EventType;
import Event.Extensions;
import Event.Payload;
import contact.CanonicalContact;
import contact.SystemId;
import nz.co.genesisenergy.common.GlobalConstants;
import nz.co.genesisenergy.common.Util;
import nz.co.genesisenergy.esb.customer.createcontact.v1.Account;
import nz.co.genesisenergy.esb.customer.createcontact.v1.AccountContactRole;
import nz.co.genesisenergy.esb.customer.createcontact.v1.CommunicationDetail;
import nz.co.genesisenergy.esb.customer.createcontact.v1.CreateContact;
import nz.co.genesisenergy.esb.customer.createcontact.v1.Role;
//import nz.co.genesisenergy.esb.customer.updatecontact.v1.Brand;

public class CMESBCreateContactTransformer  implements Callable{

	@Override
	public Object onCall(MuleEventContext eventContext) throws Exception {

    	Event muleEvent = new Event();
    	MuleMessage message = eventContext.getMessage();
		try {

			muleEvent = (Event)message.getPayload();
					
	    	CanonicalContact canonicalContactEvent = new CanonicalContact();
	    	ObjectMapper mapper = new ObjectMapper();
	    	String encodedCanonicalString = muleEvent.getPayload().getContent();
	    	String decodedCanonical = PayloadBase64Decoder.getInstance().decode(encodedCanonicalString);
			canonicalContactEvent = mapper.readValue(decodedCanonical, CanonicalContact.class);
	
	    	String createContactXML = "";
			String operation = muleEvent.getEventType().getOperation();
			//Take out in choice
			if(GlobalConstants.OPERATION_CREATE.equalsIgnoreCase(operation)){
				createContactXML = transformCreate(canonicalContactEvent, message);
				message.setInvocationProperty(GlobalConstants.PROPERTY_OPERATION, GlobalConstants.OPERATION_CREATE);
				return createContactXML;
			}else{
				throw new Exception("Wrong data. It should be a create contact event!");
			}
		} catch (Exception e) {
    		Log.error("Exception: " + e.getMessage());
			setErrorMuleEvent(message, muleEvent, e);
			throw new Exception ("Transformation Exception");
		}
    }
    
    private void setErrorMuleEvent(MuleMessage message, Event muleEvent, Exception e) {
		Event errorMuleEvent = new Event();
		errorMuleEvent.setId(muleEvent.getId());
		errorMuleEvent.setCreationTime(new Date().toString());
		Endpoint provider = new Endpoint();
		provider.setId(GlobalConstants.MULE_PROVIDER_ID);
		provider.setName(GlobalConstants.SF_CONTACT_EVENT_PROVIDER);
		errorMuleEvent.setProvider(provider);
		EventType eventType = new EventType();
		eventType.setName(muleEvent.getEventType().getName());
		eventType.setObjectType(muleEvent.getEventType().getObjectType());
		eventType.setOperation(muleEvent.getEventType().getOperation());
		errorMuleEvent.setEventType(eventType);
		Payload errorPayload = new Payload();
		errorPayload.setContent(e.getStackTrace().toString());
		message.setPayload(errorPayload);
		errorMuleEvent.setPayload(errorPayload);
		Extensions extensions = new Extensions();
    	extensions.setAdditionalProperty(GlobalConstants.LAST_PAYLOAD, muleEvent.getPayload().getContent());
    	try{
	    	String originalPayloadString = muleEvent.getExtensions().getAdditionalProperties().get(GlobalConstants.ORIGINAL_PAYLOAD);
	    	extensions.setAdditionalProperty(GlobalConstants.ORIGINAL_PAYLOAD, originalPayloadString);
    	}catch(Exception ex){
    		Log.error("No extensions with original payload available");
    	}
    	errorMuleEvent.setExtensions(extensions);
	}

    /**
     * TODO: update the hardcoded role types, when available in the next sprints
     * @param cm
     * @param message
     * @return
     * @throws ParseException
     * @throws DatatypeConfigurationException
     * @throws IOException
     * @throws JAXBException
     */
    private String transformCreate(CanonicalContact cm, MuleMessage message) throws ParseException, DatatypeConfigurationException, IOException, JAXBException{

    	CreateContact createContact = new CreateContact();
    	CreateContact.CreateContactRequest request = new CreateContact.CreateContactRequest();
//    	Brand brand = new Brand();
//    	brand.setBrand(cm.getBrand().value());
		message.setOutboundProperty(GlobalConstants.HEADER_X_BRAND_ID, cm.getBrand().value());
		message.setOutboundProperty(GlobalConstants.HEADER_X_USER_ID, cm.getLastModifiedById());
    	nz.co.genesisenergy.esb.customer.createcontact.v1.Contact v1Contact = 
    			new nz.co.genesisenergy.esb.customer.createcontact.v1.Contact();

    	AccountContactRole accountContactRole = new AccountContactRole();
    	Account accountValue = new Account();

    	// hardcoded for this sprint
    	accountValue.setId("GENE-Gentrack-903883718");
    	
    	accountContactRole.setAccount(accountValue);
    	Role role = new Role();
    	role.setRoleType(GlobalConstants.ROLE_TYPE_PRIMARY); //TODO: update the hardcoded valued in the next sprints
    	role.setSubType(GlobalConstants.ROLE_TYPE_RESIDENTIAL);  //TODO: update the hardcoded valued in the next sprints
    	accountContactRole.setRole(role);
    	v1Contact.setAccountRole(accountContactRole);
    	
    	XMLGregorianCalendar calendar = Util.getXMLDate(cm.getBirthDate());
    	v1Contact.setDateOfBirth(calendar);
    	v1Contact.setFirstName(cm.getFirstName());
    	v1Contact.setLastName(cm.getLastName());
    	v1Contact.setPreferredName(cm.getPreferredName());
    	v1Contact.setTitle(cm.getSalutation());

    	List<contact.CommunicationDetail> cmComDetails = cm.getCommunicationDetails();
    	for(contact.CommunicationDetail comDetail : cmComDetails){
    		String cmPreference = comDetail.getPreference().value();
	    	if (GlobalConstants.HOME_PREFERENCE.equals(cmPreference) && 
					 ( GlobalConstants.EMAIL_PREFERENCE.equals(comDetail.getCommunicationType().value()) || 
					   GlobalConstants.FAX_PREFERENCE.equals(comDetail.getCommunicationType().value()) ||
					   GlobalConstants.MOBILE_PREFERENCE.equals(comDetail.getCommunicationType().value()) )  ){
				 cmPreference = GlobalConstants.PRIVATE_PREFERENCE;
			 }
			 
			 CommunicationDetail cDetail = new CommunicationDetail();
			 cDetail.setType(comDetail.getCommunicationType().value());
			 cDetail.setPreference(cmPreference);
			 cDetail.setValue(comDetail.getValue());
			 //this is a reference, so no set method, just add
			 v1Contact.getCommunicationDetail().add(cDetail);
    	}
    	
    	request.setContact(v1Contact);
    	createContact.setCreateContactRequest(request);
        return jaxbObjectToXML(createContact);
    }
   
    /**
     * This will be used for role account number, but for now it's hardcoded
     * @param cm
     * @return
     */
    private Long getGentrackAccountNumber(CanonicalContact cm) {
    	List<SystemId> systemIds = cm.getSystemIds();
    	for(SystemId system : systemIds){
    		if(system.getSystem().GT.equals(system.getSystem())){
    	    	Long accountNumber = Long.parseLong(system.getId());
    			return accountNumber ;
    		}
    	}
    	
		return null;
	}

    private static String jaxbObjectToXML(CreateContact request) throws JAXBException
    {
        StringWriter sw = new StringWriter();
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(CreateContact.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE); // To format XML
 
            jaxbMarshaller.marshal(request, sw);             
        } catch (JAXBException e) {
        	throw e;
        }
        
        return sw.toString();
    }}
