package nz.co.genesisenergy.common;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

public class Util {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

    public static XMLGregorianCalendar getXMLDate(String dateInput) throws ParseException, DatatypeConfigurationException{
	    DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
	    Date date = format.parse(dateInput);
	
	    GregorianCalendar cal = new GregorianCalendar();
	    cal.setTime(date);
	
	    XMLGregorianCalendar xmlGregCal =  DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
	
	    System.out.println(xmlGregCal);
	    return xmlGregCal;
    }
}
