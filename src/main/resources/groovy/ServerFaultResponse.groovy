package groovy

import groovy.xml.MarkupBuilder

def writer = new StringWriter()
def out = new MarkupBuilder(writer)

def errorCode = flowVars['errorCode']
def errorMessage

out.'v1:createContactResponse' ( "xmlns:v1":"http://www.genesisenergy.co.nz/esb/customer/createContact/v1") {
	'v1:responseStatus' 'FAILURE'
	'v1:fault' {
		'v1:error' {
			"v1:id"(sessionVars.message_identifier)
			if(errorCode == 'MISSING_USERID'){
				'v1:code'(errorCode)
				'v1:detail'('Please supply the UserId of the user requesting this update and try again.')
			}
			else if(errorCode == 'SCHEMA_VALIDATION_FAILED'){
				'v1:code'('INVALID_REQUEST')
				'v1:detail'('Your request does not match the WSDL of this service, please check your request and try again.')
			}
			else if(errorCode == 'INVALID_REQUEST'){
				'v1:code'(errorCode)
				def invalidRequestException = flowVars['invalidRequestException']
				errorMessage = 'Invalid request: ' + (invalidRequestException==null?'':invalidRequestException.getErrorDetails())
				'v1:detail'(errorMessage)
			}
			else if(errorCode == 'NOT_IMPLEMENTED'){
				'v1:code'(errorCode)
				def notImplementedException = flowVars['notImplementedException']
				errorMessage = notImplementedException==null?'':notImplementedException.getMessage()
				'v1:detail'('Your request is valid but this service has not implemented using : ' + errorMessage)
			}
			else if(errorCode == 'TIMEOUT'){
				'v1:code'(errorCode)
				'v1:detail'('The system has timed out trying to retrieve the required information. Please try again or process the request manually.')
			}
			else if(errorCode == 'SERVER_ERROR'){
				'v1:code'(errorCode)
				'v1:detail'('The system has encountered an error trying to retrieve the required information. Please try again or process the request manually.')
			}
			else if(errorCode == 'SERVICE_UNAVAILABLE'){
				'v1:code'(errorCode)
				'v1:detail'('The server is currently unable to handle the request due to a temporary overloading or maintenance of the backend provider systems.')
			}
			else {
				//UNKNOWN_PROVIDER_EXCEPTION
				'v1:code'(errorCode)
				'v1:detail'('The system has encountered an error trying to retrieve the required information. Please try again or process the request manually.')
			}
		}
	}
}

return writer.toString()