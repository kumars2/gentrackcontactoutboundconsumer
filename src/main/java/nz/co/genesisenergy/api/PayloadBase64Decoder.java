/**
 * 
 */
package nz.co.genesisenergy.api;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.mule.api.MuleEventContext;
import org.mule.api.MuleMessage;
import org.mule.api.lifecycle.Callable;

//encoding
import java.util.Base64;

/**
 * Transform any event into Generic Mule Event
 * @author teodorc
 *
 */
public class PayloadBase64Decoder implements Callable{
	
	final Logger log = LogManager.getLogger(this.getClass().getName());
	private static PayloadBase64Decoder instance;
	public static PayloadBase64Decoder getInstance() {
		if(instance == null){
			instance = new PayloadBase64Decoder();
		}
		return instance;
	}
	public PayloadBase64Decoder(){}

	@Override
	public Object onCall(MuleEventContext eventContext) throws Exception {
		MuleMessage message = eventContext.getMessage();
		String payload = message.getPayloadAsString();

		message.setPayload(decode(payload));
		return message;
	}
	
	public String decode(String encodedString){
		byte[] decodedBytes = Base64.getDecoder().decode(encodedString);
		String decodedString = new String(decodedBytes);

		return decodedString ;		
	}


}
