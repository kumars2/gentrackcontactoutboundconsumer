/**
 * Transform to base 64
 */
package nz.co.genesisenergy.api;

import org.mule.api.MuleEventContext;
import org.mule.api.MuleMessage;
import org.mule.api.lifecycle.Callable;

//encoding
import java.util.Base64;

public class PayloadBase64Encoder implements Callable{

	@Override
	public Object onCall(MuleEventContext eventContext) throws Exception {
		MuleMessage message = eventContext.getMessage();
		String payload = message.getPayloadAsString();
		message.setPayload(encode(payload));
		return message;
	}
	
	public String encode(String originalInput )  {
		String encodedString = Base64.getEncoder().encodeToString(originalInput.getBytes());
		return encodedString;
	}


}
