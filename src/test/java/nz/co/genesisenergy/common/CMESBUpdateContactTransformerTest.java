package nz.co.genesisenergy.common;

import static org.junit.Assert.*;

import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.custommonkey.xmlunit.Diff;
import org.custommonkey.xmlunit.XMLUnit;
import org.junit.Test;
import org.mule.DefaultMuleEventContext;
import org.mule.api.MuleEvent;
import org.mule.api.MuleEventContext;
import org.mule.api.MuleMessage;
import org.mule.component.SimpleCallableJavaComponentTestCase;
import org.w3c.dom.Document;

import com.fasterxml.jackson.databind.ObjectMapper;

import Event.Event;
import nz.co.genesisenergy.api.CMESBUpdateContactTransformer;

public class CMESBUpdateContactTransformerTest  extends SimpleCallableJavaComponentTestCase {

	String muleEventPayload = null;
	MuleMessage message = null;

	// read json from resource files
	public String readFile(String fileName) throws Exception {
		URI uri = CMESBUpdateContactTransformerTest.class.getClassLoader().getResource(fileName).toURI();
		String string = new String(Files.readAllBytes(Paths.get(uri)));
		return string;
	}

	Event getCreateContactCanonical() throws Exception {
		String payloadAsString = readFile("mule-event/update-contact-mule-event.json");

    	ObjectMapper muleEventmapper = new ObjectMapper();
		Event event = muleEventmapper.readValue(payloadAsString, Event.class);
		
		return event;
	}

	String getCreateContactCM2ESBXML() throws Exception {
		System.out.println("READ FILE");
		
		String payload = readFile("cm-esb-transforms/update-contact-cm-esb.xml");
		System.out.println("PAYLOAD: " + payload);
		return payload;
	}

	/**
	 * We are using getTestMuleMessage from SimpleCallableJavaComponentTestCase
	 * 
	 * @throws Exception
	 */
	@Test
	public void testOnCall() throws Exception {
		Event inPayload = getCreateContactCanonical();
		message = getTestMuleMessage();
		message.setPayload(inPayload);

		MuleEvent event = null;
		try {
			event = getTestEvent(message, muleContext);
		} catch (Exception e1) {
			assertFalse(true);
		}
		MuleEventContext eventContext = new DefaultMuleEventContext(event);
		CMESBUpdateContactTransformer transformation = new CMESBUpdateContactTransformer();

		// this is the public test
		Object result = null;
		try {
			result = transformation.onCall(eventContext);
			System.out.println("result: " + result.toString());
			muleEventPayload = (String) result;
		} catch (Exception e) {
			assertFalse(true);
		}

		String resultString = (String)result;
		String contactESBXML = getCreateContactCM2ESBXML();

		System.out.println("ResultString: " + resultString);
		System.out.println("contactESBXML: " + contactESBXML);
		
		Document resultDocument = XMLUnit.buildTestDocument(resultString);
		Document expectedDocument = XMLUnit.buildTestDocument(contactESBXML);

		XMLUnit.setIgnoreWhitespace(true);
		boolean equals = resultDocument.equals(expectedDocument);

		Diff xmlDiff = new Diff( resultDocument, expectedDocument);
		assertTrue(xmlDiff.identical());
		
	}

}
